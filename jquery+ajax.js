var $input_DVD = $('<input id="dvd" input type="text" name="dvd" class="input" placeholder="Size in Mb"/>');
var $input_Book = $('<input id="book" input type="text" name="book" class="input" placeholder="Weight in Kg"/>');
var $input_FurnitureHeight = $('<input id="furnh" input type="text" class="input" name="furnh" placeholder="Height in Cm"/>');
var $input_FurnitureWidth = $('<input id="furnw" input type="text" class="input" name="furnw" placeholder="Width in Cm"/>');
var $input_FurnitureLength = $('<input id="furnl" input type="text" class="input" name="furnl"  placeholder="Length in Cm"/>');


$(document).ready(function() {
    $('select#Product_Type').on('change', function() {
        $('#container').remove();

        var val = $(this).val()
        $container = $('<fieldset id="container" class="inner" ></fieldset>');//  <!---- fieldset, not form

        if (val == "Dvd")$input_DVD.appendTo($container);
        if (val == "Book") $input_Book.appendTo($container);
        if (val == "Furniture"){
            $input_FurnitureHeight.appendTo($container);
            $input_FurnitureWidth.appendTo($container);
            $input_FurnitureLength.appendTo($container);
        }
        $container.insertAfter($(this)).val();
    })
})

$(function() {
    $('form').submit(function(e) {
        e.preventDefault();
        $.ajax({
            type        : 'POST',
            url         : location.href,    //  Posting data to same page
            data        : $(this).serialize(),  //  Sending all submited data
            dataType    : 'json',
            encode      : true
        })
            .done(function(data) {
                $('#result').html( data );
            })
    });
});

