<?php

if( $_SERVER['REQUEST_METHOD']=='POST' && isset( $_POST['submitted'] ) ) {

    ob_clean();

    include('connect_mysql.php');

    $message='error';

    if( isset( $_POST['price'], $_POST['sku'], $_POST['name'], $_POST['prtype'] ) ) {
        //filter posted varables
        $args=array(
            'sku'       =>  FILTER_SANITIZE_STRING,
            'name'      =>  FILTER_SANITIZE_STRING,
            'price'     =>  array(
                'filter'    => FILTER_SANITIZE_NUMBER_FLOAT,
                'flags'     => FILTER_FLAG_ALLOW_FRACTION | FILTER_FLAG_ALLOW_THOUSAND
            ),
            'prtype'    =>  FILTER_SANITIZE_STRING,
            'dvd'       =>  FILTER_SANITIZE_STRING,
            'book'      =>  FILTER_SANITIZE_STRING,
            'furnh'     =>  FILTER_SANITIZE_STRING,
            'furnw'     =>  FILTER_SANITIZE_STRING,
            'furnl'     =>  FILTER_SANITIZE_STRING
        );
        $_POST=filter_input_array( INPUT_POST, $args );
        //extract data to variable
        extract( $_POST );

        //query to every record
        $sql = "insert into `product_list` ( `sku`, `name`, `price`, `prtype`, `dvd`, `book`, `furnh`, `furnw`, `furnl` ) values (?,?,?,?,?,?,?,?,?)";
        header("location:add_product.php");
        $stmt= $dbcon->prepare( $sql );

        if( $stmt ){

            $stmt->bind_param('ssdssssss', $sku, $name, $price, $prtype, $dvd, $book, $furnh, $furnw, $furnl );

             //setting emty values to the field wich are not selected

            if( !empty( $_POST['dvd'] ) ){

                $book = $furnh = $funw = $furnl = '';

            } elseif( !empty( $_POST['book'] ) ){

                $dvd = $furnh = $funw = $furnl = '';

            } elseif( isset( $_POST['furnw'],$_POST['furnl'],$_POST['furnh'] ) ){

                $book = $dvd = '';
            }

            $result=$stmt->execute();
            $stmt->free_result();
            $message=$result ? '1 record added' : 'Something went wrong';

        } else {
            $message='Problem preparing sql statement';
        }

        exit( $message );
    }
}
?>
<html>
<head>
    <title>Submit your data</title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <link href="style.css"  rel="stylesheet">
    <script src="jquery+ajax.js"></script>
</head>
<body>
<div id="mainform">
<h1>Insert your data</h1>
<form method="post" action="add_product.php">
    <input type="hidden" name="submitted" value="true" />

    <fieldset id="product_list">
        <legend>New Product</legend>
        <label>SKU:<input type="text" name="sku"/></label>
        <label>Name:<input type="text" name="name"/></label>
        <label>Price:<input type="text" name="price"/></label>
        <label>Product Type</label>
        <select id="Product_Type" name="prtype">
            <option style="display: none;" selected>Select product type</option>
            <option value="Dvd">DVD</option>
            <option value="Book">Book</option>
            <option value="Furniture">Furniture</option>
        </select>
    </fieldset>
</div>
    <br  />
    <input type="submit" name="add" value="Add" onClick="window.location.reload()"/>
</form>
<div id='result'></div>
<button id="back" class="back" onclick="location.href='index.html';">Go Back</button>
</body>
</html>